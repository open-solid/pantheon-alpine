# Pantheon Alpine

This Docker image contains [Pantheon](https://github.com/PegaSysEng/pantheon) in Alpine.

The Docker image is built using multi-stage builds that also improve readibility and size.

## Commands

Get the latest version for the docker registry

> docker pull edsonalcala/pantheon:1.0.2-alpine

## Usage

You can simply run:

> docker run edsonalcala/pantheon:1.0.2-alpine

If you want to deploy a Pantheon network using this image you can check this Pantheon implementation for 4 nodes:

https://github.com/EdsonAlcala/pantheon-n-nodes

## References

https://docs.pantheon.pegasys.tech/en/latest/Installation/Install-Binaries/

https://docs.pantheon.pegasys.tech/en/latest/Getting-Started/Starting-Pantheon/