FROM gradle:5.3.1-jdk8-alpine as pantheon-builder
USER root
ARG VERSION=1.0.2

RUN \
    apk add --update linux-headers ca-certificates wget && \
    wget https://bintray.com/consensys/pegasys-repo/download_file?file_path=pantheon-${VERSION}.tar.gz -O pantheon-${VERSION}.tar.gz && \
    tar -xzf pantheon-${VERSION}.tar.gz && mv pantheon-${VERSION} /pantheon

FROM openjdk:8-jre-alpine

COPY --from=pantheon-builder /pantheon/lib /pantheon/lib
COPY --from=pantheon-builder /pantheon/bin/pantheon /pantheon/bin/pantheon

ENV PATH="/pantheon/bin:${PATH}"

ENTRYPOINT [ "pantheon" ]

CMD [ "--help" ]